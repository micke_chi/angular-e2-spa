import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HeroesService } from "../../services/heroes.service";

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe:any={};
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _heroesService: HeroesService
  ) {

    this._activatedRoute.params.subscribe( params => {
      this.heroe = _heroesService.getHeroe(params['id']);
        console.log("los parametros", this.heroe);
    } )
  }

  ngOnInit() {
  }


}
