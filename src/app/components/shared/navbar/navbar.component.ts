import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private _router : Router) { }

  ngOnInit() {
  }

  buscarHeroe( termino:string ){
    if(termino){
      this._router.navigate(['/heroes', termino]);
    }else{
      this._router.navigate(['/heroes']);
    }
  }

}
