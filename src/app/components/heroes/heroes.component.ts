import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from "../../services/heroes.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  termino:string = undefined;
  heroes:Heroe[] = [];
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _heroesService: HeroesService,
  ) {
    this._activatedRoute.params.subscribe((params)=>{
      this.termino = params['termino'];
      console.log("parametros en heroes", this.termino);
      this.obtenerHeroesLocales();

    });
  }

  ngOnInit() {
    console.log("lanzando ng init", this.termino);
    //this.obtenerHeroesLocales();
  }

  obtenerHeroesLocales(){
    console.log("parametros en buscar heroes", this.termino);
    this.heroes = (typeof this.termino === "undefined")? this._heroesService.getHeroes() : this._heroesService.buscarHeroes(this.termino);
  }

}
